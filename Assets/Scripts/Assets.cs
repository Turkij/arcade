﻿using UnityEngine;
using System.Collections;

public class Assets : MonoBehaviour {
    public GameObject player;
	public GameObject enemy_bee;
	public GameObject enemy_grabber;
	public GameObject enemy_butterfly;
	public GameObject backdrop;
	public GameObject playerbullet;
	public GameObject enemybullet;
	public AnimationClip deathAnimation;
}
