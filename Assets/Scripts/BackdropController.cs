﻿using UnityEngine;
using System.Collections;

public class BackdropController : MonoBehaviour {

	public float scrollSpeed = 1;
	public float height = 20;
	public bool spawnedNew = false;
	public GameObject nextBackdrop;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		Transform transform = GetComponent<Transform>();
		transform.position += new Vector3(0, Time.deltaTime * -scrollSpeed, 0);
        if((transform.position.y + height/2) < GameController.topOfScreen){
			if(!spawnedNew){
				Debug.Log ("3");
            	GameObject g = Instantiate(nextBackdrop);
				g.GetComponent<BackdropController>().nextBackdrop = nextBackdrop;
				g.GetComponent<Transform>().position = new Vector3(0, GameController.topOfScreen + height/2 - 0.1f, 0);
				spawnedNew = true;
			}else{
				if((transform.position.y + height/2) < GameController.bottomOfScreen){
					Destroy(gameObject);
				}
			}
        }
	}
}
