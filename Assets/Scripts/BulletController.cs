﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {

	public float xSpeed;
	public float ySpeed;

	void Awake(){

	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Transform> ().position += new Vector3 (xSpeed, ySpeed);
		float y = GetComponent<Transform> ().position.y;
		if (y > GameController.topOfScreen || y < GameController.bottomOfScreen) {
			if (name == "PlayerBullet(Clone)") {
				GameController.G.numPlayerBullets--;
			}
			Destroy(gameObject);
		}
	}
	
}
