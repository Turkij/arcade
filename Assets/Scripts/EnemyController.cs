﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	Coroutine currentPathCoroutine;
	public int xSlot = -1, ySlot = -1;
	public bool isDead = false;

	// Use this for initialization
	public virtual void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void DoPath(Path p){
		if (!isDead) {
			if (currentPathCoroutine != null) {
				StopCoroutine (currentPathCoroutine);
			}
			currentPathCoroutine = StartCoroutine (DoPathHelper (p));
		}
	}

	private IEnumerator DoPathHelper(Path p){
		while (!p.IsDone() ) {
			Vector3 oldPos = GetComponent<Transform>().position;
			Vector3 newPos = p.GetCoords();
			GetComponent<Transform>().position = p.GetCoords();
			float degrees;
			if(Mathf.Abs(newPos.x-oldPos.x) < 0.000001){
				degrees = 180;
			}else{
				degrees = Mathf.Rad2Deg*Mathf.Atan2(newPos.y-oldPos.y, newPos.x-oldPos.x) - 90;
			}
			GetComponent<Transform>().eulerAngles = new Vector3(0, 0, degrees);
			if(p.DoesShoot()){
				GameObject bullet = (GameObject)Instantiate(GameController.G.Assets.enemybullet, transform.position, Quaternion.identity);
				bullet.GetComponent<BulletController>().xSpeed = UnityEngine.Random.Range(-0.01f, 0.01f);
			}else if(p.DoesSlotIn() ){
				GameController.G.slotIn(xSlot, ySlot, gameObject);
			}
			p.Increment();
			yield return null;
		}
	}

	protected virtual void OnTriggerEnter2D(Collider2D other){
		if (!isDead) {
			if (other.name == "PlayerBullet(Clone)") {
				GameController.G.numPlayerBullets--;
				Destroy (other.gameObject);
				Kill ();
			} else if (other.name == "PlayerShip(Clone)") {
				Kill ();
				if(other != null){
					other.GetComponent<PlayerController> ().Kill ();
				}
			}
		}
	}

	public void Kill(){
		if (xSlot != -1 && ySlot != -1) {
			GameController.G.slotOut (xSlot, ySlot);
		}
		isDead = true;
		GameController.G.allEnemies.Remove (gameObject);
		GetComponent<Animator> ().SetBool ("isDead", true);
		if (currentPathCoroutine != null) {
			StopCoroutine (currentPathCoroutine);
		}
		Destroy (gameObject, GameController.G.Assets.deathAnimation.length);
	}

}
