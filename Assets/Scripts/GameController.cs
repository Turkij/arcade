﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    public Assets Assets;
    public static GameController G;
	public static int topOfScreen = 5;
	public static int bottomOfScreen = -5;
	public static int leftOfScreen = -5;
	public static int rightOfScreen = 5;
	public int numPlayerBullets = 0;
	public int level;
	public int livesLeft;
	public int waveTimerAvg;
	public int waveTimerDev;
	public int waveTimer;
	public int subWaveTimerAvg;
	public int subWaveTimerDev;
	public int subWaveTimer;
	public int enemiesPerWave;
	public int waveEnemiesLeft;
	public bool settingUp;
	public int shotsPerSubWave;
	private GameObject player;
	public List<GameObject> allEnemies;
	public GUIText splash;
	//public int enemiesLeft;

	protected GameObject[,] slots;
	protected List<GameObject> inSlots;
	protected const float X_OFFSET_DIST = 0.05f;
	protected const float STD_X_DIST = 0.35f;
	protected const float Y_OFFSET_DIST = 0.02f;
	protected const float STD_Y_DIST = 0.35f;
	protected const float XCENTER = 0f;
	protected const float YCENTER = 4.5f;
	protected const int NUM_CYCLES = 4;
	protected int cycle;
	protected const int TOTAL_SLOT_DELAY = 100;
	protected int slotDelay;
	protected bool expanding;

    void Awake()
    {
        G = this;
		slots = new GameObject[12,5];
		inSlots = new List<GameObject> ();
		expanding = true;
		cycle = 0;
		slotDelay = TOTAL_SLOT_DELAY;
		allEnemies = new List<GameObject> ();
    }

	// Use this for initialization
	void Start () {
		waveTimer = 100;
		subWaveTimer = 0;
		level = 0;
		livesLeft = 2;
		player = (GameObject)Instantiate (Assets.player, new Vector3 (PlayerController.startx, PlayerController.starty), Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.R)) {
			print ("hi");
			Application.LoadLevel("menu");
		}
		if (allEnemies.Count == 0 && !settingUp ) {
			++level;
			//display that you won
			StartCoroutine(SetupLevel());
		} else {
			if (slotDelay <= 0) {
				DoSlotStuff ();
				slotDelay = TOTAL_SLOT_DELAY;
			} else {
				--slotDelay;
			}
			waveTimer--;
			if (waveTimer <= 0) {
				waveTimer = waveTimerAvg + (int)UnityEngine.Random.Range (-waveTimerDev, waveTimerDev);
				waveEnemiesLeft = enemiesPerWave;
			}
			if (waveEnemiesLeft > 0) {
				subWaveTimer--;
				if (subWaveTimer <= 0 && player != null && player.activeSelf) {
					if (sendEnemy ()) {
						subWaveTimer = subWaveTimerAvg + (int)UnityEngine.Random.Range (-subWaveTimerDev, subWaveTimerDev);
						waveEnemiesLeft--;
					}
				}
			}
		}
	}

	protected virtual void DoSlotStuff(){
		for (int y=0; y<slots.GetLength (1); ++y) {
			for (int x=0; x<slots.GetLength (0); ++x) {
				if (slots [x, y] != null) {
					int distFromCenter = 2 * x - slots.GetLength (0)+1;
					float xCoord = XCENTER + distFromCenter * (STD_X_DIST + cycle * X_OFFSET_DIST);
					float yCoord = YCENTER - (2*y+1) * (STD_Y_DIST + cycle*Y_OFFSET_DIST);
					slots [x, y].GetComponent<Transform> ().position = new Vector3 (xCoord, yCoord);
					slots[x, y].GetComponent<Transform>().eulerAngles = new Vector3(0, 0, 0);
				}
			}
		}
		if (expanding) {
			++cycle;
			if (cycle == NUM_CYCLES - 1) {
				expanding = false;
			}
		} else {
			--cycle;
			if (cycle == 0) {
				expanding = true;
			}
		}
	}

	public virtual bool sendEnemy(){
		if (inSlots.Count > 0) {
			GameObject sending = inSlots [(int)UnityEngine.Random.Range (0, inSlots.Count - 1)];
			int xCoord = -1, yCoord = -1;
			for (int x=0; x<slots.GetLength(0); x++) {
				for (int y=0; y<slots.GetLength(1); y++) {
					if (slots [x, y] == sending) {
						xCoord = x;
						yCoord = y;
					}
				}
			}
			slotOut (xCoord, yCoord);
			if (sending == null) {
				Debug.Log ("note - reference made to dead object");
			}else{
				EnemyController enemy = sending.GetComponent<EnemyController> ();
				Vector3 pos = sending.GetComponent<Transform>().position;
				float x1 = UnityEngine.Random.Range(PlayerController.leftBound, PlayerController.rightBound);
				float y1 = 0;
				float x2 = UnityEngine.Random.Range(PlayerController.leftBound, PlayerController.rightBound);
				float y2 = -2;
				bool randBool = UnityEngine.Random.value < 0.5f ? false : true;
				Path p = Path.GenerateHalfLoop(pos.x, pos.y, 2, !randBool, 50);
				p.concat( Path.GenerateCurvyPath(100, p.getLastCoords()[0], p.getLastCoords()[1], x1, y1));
				if(UnityEngine.Random.value < 0.5f){
					p.concat (Path.GenerateLoop(x1, y1, 2, randBool, 70) );
				}
				p.concat(Path.GenerateCurvyPath(70, x1, y1, x2, y2));
				if(UnityEngine.Random.value < 0.5f){
					p.concat (Path.GenerateLoop(x2, y2, 2, !randBool, 100) );
				}
				p.concat(Path.GenerateLinearPath(x2, y2, x2, bottomOfScreen - 0.5f, 50));
				float[] dest = predictSlotPos(xCoord, yCoord, 50);
				p.concat(Path.GenerateLinearPath(dest[0], topOfScreen + 0.5f, dest[0], dest[1], 50));
				for(int i=0;i<shotsPerSubWave;i++){
					p.AddShoot((int)(UnityEngine.Random.value * p.length() ));
				}
				p.AddSlotIn();
				enemy.DoPath(p);
			}
			return true;
		} else {
			return false;
		}
	}

	public float[] predictSlotPos(int x, int y, int updates){
		int distFromCenter = 2 * x - slots.GetLength (0)+1;
		float[] result = {XCENTER + distFromCenter * (STD_X_DIST + cycle * X_OFFSET_DIST), YCENTER - (2*y+1) * (STD_Y_DIST + cycle*Y_OFFSET_DIST)};
		return result;
	}

	public void slotIn(int x, int y, GameObject g){
		g.GetComponent<EnemyController> ().xSlot = x;
		g.GetComponent<EnemyController> ().ySlot = y;
		slots [x, y] = g;
		inSlots.Add (g);
	}
	public void slotOut(int x, int y){
		if (slots [x,y] != null) {
			inSlots.Remove (slots [x,y]);
			slots[x,y] = null;
		}
	}

	public virtual void LoseLife(){
		livesLeft--;
		if (livesLeft == 0) {
			//display stuff

			livesLeft = 2;
			level = 1;
			StartCoroutine (SetupLevel ());
		} else {
			StartCoroutine(DelayedRespawn(6));
		}
	}

	public IEnumerator DelayedRespawn(float seconds){
		player = (GameObject)Instantiate (Assets.player, new Vector3 (PlayerController.startx, PlayerController.starty), Quaternion.identity);
		player.SetActive (false);
		yield return new WaitForSeconds(seconds);
		player.SetActive (true);
	}

	public virtual IEnumerator SetupLevel(){
		settingUp = true;
		yield return new WaitForSeconds (2);
		if (player == null || !player.activeSelf) {
			player = (GameObject)Instantiate (Assets.player, new Vector3 (PlayerController.startx, PlayerController.starty), Quaternion.identity);
		}
		while (allEnemies.Count > 0) {
			Destroy (allEnemies [0]);
			allEnemies.RemoveAt (0);
		}
		while (inSlots.Count > 0) {
			inSlots.RemoveAt (0);
		}
		for (int y=0; y<slots.GetLength (1); ++y) {
			for (int x=0; x<slots.GetLength (0); ++x) {
				slots [x, y] = null;
			}
		}

		Destroy (splash);
		SetWaveValues ();

		for (int x=slots.GetLength(0)/2-2; x<slots.GetLength(0)/2+2; ++x) {
			GameObject g = Instantiate(Assets.enemy_grabber);
			allEnemies.Add(g);
			slotIn(x, 0, g);
		}
		for (int y=1; y<slots.GetLength (1)-2; ++y) {
			for (int x=0; x<slots.GetLength (0); ++x) {
				GameObject g = Instantiate(Assets.enemy_bee);
				allEnemies.Add(g);
				slotIn(x, y, g);
			}
		}
		for (int y=slots.GetLength (1)-2; y<slots.GetLength(1); ++y) {
			for (int x=0; x<slots.GetLength (0)/2-2; ++x) {
				GameObject g = Instantiate(Assets.enemy_butterfly);
				allEnemies.Add(g);
				slotIn(x, y, g);
			}
			for (int x=slots.GetLength (0)/2+2; x<slots.GetLength (0); ++x) {
				GameObject g = Instantiate(Assets.enemy_butterfly);
				allEnemies.Add(g);
				slotIn(x, y, g);
			}
		}
		DoSlotStuff ();
		settingUp = false;
	}
	
	public virtual void SetWaveValues(){
		waveTimerAvg = (int)(400-400/(1+Mathf.Pow (1.5f, level+9)));
		waveTimerDev = waveTimerAvg/8;
		subWaveTimerAvg = (int)(50-50/(1+Mathf.Pow (1.5f, -level+9)));
		subWaveTimerDev = subWaveTimerAvg;
		enemiesPerWave = level/3+1;
		waveEnemiesLeft = 0;
		shotsPerSubWave = level/3+1;
	}


}
