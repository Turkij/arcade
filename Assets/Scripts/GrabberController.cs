﻿using UnityEngine;
using System.Collections;

public class GrabberController : EnemyController {

	public bool beenHit = false;
	public Color hurtColor;

	// Use this for initialization
	void Start () {
		base.Start();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	protected override void OnTriggerEnter2D(Collider2D other){
		if (other.name == "PlayerBullet(Clone)") {
			GameController.G.numPlayerBullets--;
			Destroy(other.gameObject);
			if(beenHit){
				GetComponent<SpriteRenderer>().color = Color.white;
				base.Kill ();
			}else{
				GetComponent<SpriteRenderer>().color = hurtColor;
				beenHit = true;
			}
		}
	}
}
