﻿using UnityEngine;
using System.Collections;

public class HardModeGameController : GameController {

	public override void SetWaveValues(){
		waveTimerAvg = 10;
		waveTimerDev = 2;
		subWaveTimerAvg = 2;
		subWaveTimerDev = subWaveTimerAvg;
		enemiesPerWave = level;
		waveEnemiesLeft = 0;
		shotsPerSubWave = level+1;
	}

}
