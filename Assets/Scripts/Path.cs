﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Path {
	
	private List<float[]> coords;
	private List<int> special;
	private int index;

	public Path(){
		coords = new List<float[]> ();
		special = new List<int> ();
		index = 0;
	}

	public void AddStep(float x, float y){
		AddStep (x, y, false);
	}
	public void AddStep(float x, float y, bool shoot){
		float[] newCoords = {x, y};
		coords.Add (newCoords);
		if (shoot) {
			this.special.Add (1);
		} else {
			this.special.Add (0);
		}
	}
	public void AddShoot(int index){
		special [index] = 1;
	}
	public void AddSlotIn(){
		special [special.Count - 1] = 2;
	}

	public void Increment(){
		++index;
	}

	public Vector3 GetCoords(){
		return new Vector3(coords[index][0], coords[index][1]);
	}

	public float[] getLastCoords(){
		float[] result = (float[])coords[coords.Count-1].Clone();
		return result;
	}

	public bool DoesShoot(){
		return special [index] == 1;
	}

	public bool DoesSlotIn(){
		return special [index] == 2;
	}

	public bool IsDone(){
		return index >= coords.Count;
	}

	public int length(){
		return coords.Count;
	}

	public void concat(Path other){
		for (int i=0; i<other.coords.Count; ++i) {
			this.coords.Add(other.coords[i]);
			this.special.Add(other.special[i]);
		}
	}

	public static Path GenerateLinearPath(float x1, float y1, float x2, float y2, int length){
		Path p = new Path ();
		float xIncrement = (x2-x1)/(length-1);
		float yIncrement = (y2-y1)/(length-1);
		for(int i=1;i<length;++i){
			p.AddStep(x1+xIncrement*i, y1+yIncrement*i);
		}
		p.AddStep(x2, y2);
		return p;
	}

	public static Path GenerateWait(float x, float y, int length){
		Path p = new Path ();
		for (int i=0; i<length; ++i) {
			p.AddStep(x, y);
		}
		return p;
	}

	public static Path GenerateLoop(float x1, float y1, float diameter, bool goRight, int length){
		Path p = new Path();
		float circ = Mathf.PI * diameter;
		float rad = diameter / 2;
		float distPerStep = circ/length;
		int xdir = goRight ? 1 : -1;
		for (int i=1; i<length-1; ++i) {
			float x = x1 + xdir * (rad + rad * -Mathf.Cos(Mathf.PI * 2 / length * i));
			float y = y1 + rad * -Mathf.Sin(Mathf.PI * 2 / length * i);
			p.AddStep(x, y);
		}
		p.AddStep (x1, y1);
		return p;
	}

	public static Path GenerateHalfLoop(float x1, float y1, float diameter, bool goRight, int length){
		Path p = new Path();
		float circ = Mathf.PI * diameter;
		float rad = diameter / 2;
		float distPerStep = circ/length;
		int xdir = goRight ? 1 : -1;
		for (int i=1; i<length-1; ++i) {
			float x = x1 + xdir * (rad + rad * -Mathf.Cos(Mathf.PI  / length * i));
			float y = y1 + rad * Mathf.Sin(Mathf.PI  / length * i);
			p.AddStep(x, y);
		}
		return p;
	}

	public static Path GenerateCurvyPath(int length, float x1, float y1, params float[] outerPoints){
		Path p = new Path();
		float lastX = x1;
		float lastY = y1;
		int stepsPerOuterPoint = length / (outerPoints.Length/2);
		for(int i=0;i+1<outerPoints.Length;i += 2){
			float yincrement = (outerPoints[i+1]-lastY)/stepsPerOuterPoint;
			for(int step=0;step < stepsPerOuterPoint-1;++step){
				float y = lastY + step*yincrement;
				float x = lastX + (outerPoints[i]-lastX)/2 + (lastX-outerPoints[i])/2*Mathf.Cos((y-lastY)*Mathf.PI/(outerPoints[i+1]-lastY));
				p.AddStep(x, y);
			}
			p.AddStep(outerPoints[i], outerPoints[i+1]);
			lastY = outerPoints[i+1];
			lastX = outerPoints[i];
		}
		return p;
	}
}
