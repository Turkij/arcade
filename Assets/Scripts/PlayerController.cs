﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed;
	public static float leftBound = -6;
	public static float rightBound = 6;
	public static float startx = 0f;
	public static float starty = -4f;
	public float bulletSpeed;
	public bool isDead = false;

	// Use this for initialization
	void Start () {
		speed = 0.1f;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isDead) {
			int dir = 0;
			if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
				dir += -1;
			}
			if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) {
				dir += 1;
			}
			Transform transform = GetComponent<Transform> ();
			float newX = clamp (transform.position.x + dir * speed, leftBound, rightBound);
			transform.position = new Vector3 (newX, transform.position.y, transform.position.z);

			if (Input.GetKeyDown (KeyCode.Space) && GameController.G.numPlayerBullets < 2 && !GameController.G.settingUp) {
				GameObject bullet = (GameObject)Instantiate (GameController.G.Assets.playerbullet, transform.position, transform.rotation);
				GameController.G.numPlayerBullets++;
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (!isDead) {
			if (other.name == "EnemyBullet(Clone)") {
				Destroy (other.gameObject);
				Kill ();
			}
		}
	}

	public void Kill(){
		GameController.G.LoseLife();
		isDead = true;
		GetComponent<Animator> ().SetBool ("isDead", true);
		GetComponent<Transform> ().localScale = new Vector3 (6, 6, 1);
		Destroy(gameObject, GameController.G.Assets.deathAnimation.length);
	}

	static float clamp(float f, float min, float max){
		if (f > max) {
			f = max;
		}
		if (f < min) {
			f = min;
		}
		return f;
	}
}
