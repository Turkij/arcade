﻿using UnityEngine;
using System.Collections;

public class Selector : MonoBehaviour {

	public GameObject[] options;
	public int selected;
	// Use this for initialization
	void Start () {
		SetSelected (selected);

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.DownArrow) || Input.GetKeyDown (KeyCode.S)) {
			SetSelected((selected+1)%options.Length);
		} else if (Input.GetKeyDown (KeyCode.UpArrow) || Input.GetKeyDown (KeyCode.W)) {
			SetSelected(selected == 0 ? options.Length-1 : selected - 1);
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			options[selected].GetComponent<LoadLevelButton>().Activate();
		}
	}

	private void SetSelected(int n){
		selected = n;
		GetComponent<Transform> ().position = options [selected].GetComponent<Transform> ().position;
	}
}
