﻿using UnityEngine;
using System.Collections;

public class Special1GameController : GameController {

	public override void SetWaveValues(){
		waveTimerAvg = (int)(400-400/(1+Mathf.Pow (1.5f, level+9)));
		waveTimerDev = waveTimerAvg/8;
		subWaveTimerAvg = (int)(50-50/(1+Mathf.Pow (1.5f, -level+9)));
		subWaveTimerDev = subWaveTimerAvg;
		enemiesPerWave = 1;
		waveEnemiesLeft = 0;
		shotsPerSubWave = level/3+1;
	}

	public override bool sendEnemy(){
		if (inSlots.Count > 0) {
			float x1 = UnityEngine.Random.Range(PlayerController.leftBound, PlayerController.rightBound);
			float y1 = 0;
			float x2 = UnityEngine.Random.Range(PlayerController.leftBound, PlayerController.rightBound);
			float y2 = -2;
			bool randBool = UnityEngine.Random.value < 0.5f ? false : true;
			bool loop1 = UnityEngine.Random.value < 0.5f ? false : true;
			bool loop2 = UnityEngine.Random.value < 0.5f ? false : true;
			for (int x=0; x<slots.GetLength(0); x++) {
				for (int y=0; y<slots.GetLength(1); y++) {
					if (slots [x, y] != null) {
						EnemyController enemy = slots[x,y].GetComponent<EnemyController> ();
						Vector3 pos = slots[x,y].GetComponent<Transform>().position;

						Path p = Path.GenerateHalfLoop(pos.x, pos.y, 2, !randBool, 50);
						p.concat( Path.GenerateCurvyPath(100, p.getLastCoords()[0], p.getLastCoords()[1], x1, y1));
						if(loop1){
							p.concat (Path.GenerateLoop(x1, y1, 2, randBool, 70) );
						}
						p.concat(Path.GenerateCurvyPath(70, x1, y1, x2, y2));
						if(loop2){
							p.concat (Path.GenerateLoop(x2, y2, 2, !randBool, 100) );
						}
						p.concat(Path.GenerateLinearPath(x2, y2, x2, bottomOfScreen - 0.5f, 50));
						float[] dest = predictSlotPos(x, y, 50);
						p.concat(Path.GenerateLinearPath(dest[0], topOfScreen + 0.5f, dest[0], dest[1], 50));
						for(int i=0;i<shotsPerSubWave;i++){
							p.AddShoot((int)(UnityEngine.Random.value * p.length() ));
						}
						p.AddSlotIn();
						slotOut (x, y);
						enemy.DoPath(p);
					}
				}
			}
			return true;
		} else {
			return false;
		}
	}

}
